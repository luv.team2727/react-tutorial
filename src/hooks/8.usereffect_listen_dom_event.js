import React, { useState, useEffect } from "react";

function UseEffectListenDomEvent() {
  // useEffect(callback, [deps])
  // usecases
  // 1. useEffect(callback)
  // - Call callback after component re-render
  // - Call callback after component add element into DOM
  // 2. useEffect(callback, [])
  // 3. useEffect(callback, [deps])
  // ===============================
  // 1. Callback always is called when component mounted
  // 2. Cleanup Funtion is called before unmounted

  const [title, setTitle] = useState("");
  const [posts, setPosts] = useState([]);
  const [showGoTop, setShowGoTop] = useState(false);

  // Call API
  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((res) => res.json())
      .then((posts) => {
        setPosts(posts);
      });
  }, []);

  useEffect(() => {
    const handleScroll = () => {
      setShowGoTop(window.scrollY >= 200);
    };

    window.addEventListener("scroll", handleScroll);
    console.log("addEventListener");

    // clean function
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  //
  return (
    <div>
      <input value={title} onChange={(e) => setTitle(e.target.value)} />
      <ul>
        {posts.map((post) => {
          return <li key={post.id}> {post.title}</li>;
        })}

        {showGoTop && (
          <button style={{ position: "fixed", right: 20, bottom: 20 }}>
            Go to top
          </button>
        )}
      </ul>
    </div>
  );
}

export default UseEffectListenDomEvent;
