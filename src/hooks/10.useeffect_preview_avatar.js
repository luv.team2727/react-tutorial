import React, { useState, useEffect } from "react";

function UseEffectPreviewAvatar() {
  // useEffect(callback, [deps])
  // usecases
  // 1. useEffect(callback)
  // - Call callback after component re-render
  // - Call callback after component add element into DOM
  // 2. useEffect(callback, [])
  // 3. useEffect(callback, [deps])
  // ===============================
  // 1. Callback always is called when component mounted
  // 2. Cleanup Funtion is called before unmounted
  // 3. Cleanup Function is called before callback

  const [count, setCount] = useState(0);
  const [avatar, setAvatar] = useState();

  // cleanup
  useEffect(() => {
    console.log(`Mount or re-render::${count}`);
    // cleanup function
    return () => {
      console.log(`Cleanup::${count}`);
    };
  }, [count]);

  useEffect(() => {
    return () => {
      avatar && URL.revokeObjectURL(avatar.preview);
    };
  }, [avatar]);

  const handlePrevAvatar = (e) => {
    const file = e.target.files[0];

    file.preview = URL.createObjectURL(file);

    console.log(file.preview);

    e.target.value = null;
    setAvatar(file);
  };

  return (
    <div>
      <h1>{count}</h1>
      <button onClick={() => setCount(count + 1)}> Click me!</button>

      <h1></h1>
      <input type="file" onChange={handlePrevAvatar}></input>
      {avatar && <img src={avatar.preview} alt="" width="80%" />}
    </div>
  );
}

export default UseEffectPreviewAvatar;
