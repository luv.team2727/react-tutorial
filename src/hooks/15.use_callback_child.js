import React, { useState, memo, useRef } from "react";

function UseCallbackChild({ onStart, onStop }) {
  console.log("re-render in UseCallbackChild component ");

  return (
    <div>
      <button onClick={onStart}>Start</button>
      <button onClick={onStop}>Stop</button>
    </div>
  );
}

export default memo(UseCallbackChild);
