import React, { useState } from "react";

function UserState2WayBinding() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");

  console.log(name);

  const handleSubmit = () => {
    console.log({
      name,
      email,
    });
  };

  return (
    <div className="App" style={{ padding: 32 }}>
      <input value={name} onChange={(e) => setName(e.target.value)}></input>
      <input value={email} onChange={(e) => setEmail(e.target.value)}></input>
      <button onClick={handleSubmit}>Register</button>
    </div>
  );
}

export default UserState2WayBinding;
