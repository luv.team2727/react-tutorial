import React, { useState, useReducer } from "react";

// useState
// 1. init state
// 2. Action: down(state -1), up(state + 1)

// useReducer
// 1. init state
// 2. Action: down(state -1), up(state + 1)
// 3. Reducer
// 4. Dispatch

// Init state
const initState = 0;

// Actions
const UP_ACTION = "up";
const DOWN_ACTION = "down";

// reducer
const reducer = (state, action) => {
  console.log("Reducer running");
  switch (action) {
    case UP_ACTION:
      return state + 1;
    case DOWN_ACTION:
      return state - 1;
    default:
      throw new Error("Invalid action");
  }
};

function UseReducer() {
  const [count, setCount] = useState(0);

  const [countReducer, dispatch] = useReducer(reducer, initState);

  return (
    <div style={{ padding: "10px 32px" }}>
      <h1>UseState :: {count}</h1>
      <button onClick={() => setCount(count - 1)}> Down</button>
      <button onClick={() => setCount(count + 1)}> Up</button>

      <br />
      <h1>UseReducer :: {countReducer}</h1>
      <button onClick={() => dispatch(DOWN_ACTION)}> Down</button>
      <button onClick={() => dispatch(UP_ACTION)}> Up</button>
    </div>
  );
}

export default UseReducer;
