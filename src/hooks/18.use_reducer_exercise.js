import React, { useState, useReducer, useRef } from "react";

// useState
// 1. init state
// 2. Action: down(state -1), up(state + 1)

// useReducer
// 1. init state
// 2. Action: down(state -1), up(state + 1)
// 3. Reducer
// 4. Dispatch

// Init state
const initState = {
  job: "",
  jobs: [],
};

// Actions
const SET_JOB = "set_job";
const ADD_JOB = "add_job";
const DEL_JOB = "del_job";

const setJob = (payload) => {
  return {
    type: SET_JOB,
    payload,
  };
};

const addJob = (payload) => {
  return {
    type: ADD_JOB,
    payload,
  };
};

const delJob = (payload) => {
  return {
    type: DEL_JOB,
    payload,
  };
};
// reducer
const reducer = (state, action) => {
  console.log(`Action::${action}`);
  switch (action.type) {
    case SET_JOB:
      return {
        ...state,
        job: action.payload,
      };
    case ADD_JOB:
      return {
        ...state,
        jobs: [...state.jobs, action.payload],
      };
    case DEL_JOB:
      const newJobs = [...state.jobs];

      newJobs.splice(action.payload, 1);

      return {
        ...state,
        jobs: newJobs,
      };
    default:
      throw new Error("Invalid action");
  }
};

function UseReducerExercise() {
  const [state, dispatch] = useReducer(reducer, initState);
  const inputRef = useRef();
  const { job, jobs } = state;

  const handleSubmit = () => {
    dispatch(addJob(job));
    dispatch(setJob(""));
    inputRef.current.focus();
    console.log(state);
  };

  return (
    <div style={{ padding: "10px 32px" }}>
      <h1>Todo</h1>
      <input
        ref={inputRef}
        value={job}
        placeholder="Enter todo"
        onChange={(e) => {
          dispatch(setJob(e.target.value));
        }}
      ></input>
      <br />
      <button onClick={handleSubmit}> Add</button>
      <ul>
        {jobs.map((job, index) => {
          return (
            <li key={index}>
              {job}{" "}
              <span
                onClick={() => {
                  dispatch(delJob(index));
                }}
              >
                &times;
              </span>
            </li>
          );
        })}
      </ul>
    </div>
  );
}

export default UseReducerExercise;
