import UserState from "./1.usestate";
import UserState2WayBinding from "./2.usestate_two_way_binding";
import UserStateList from "./3.userstate_list";
import UseStateExercise from "./4.usestate_exercise";
import UseEffectUpdateDom from "./5.useeffect_update_dom";
import UseEffectCallAPI from "./6.useeffect_call_api";
import UseEffectDepenency from "./7.useeffect_dependence";
import UseEffectListenDomEvent from "./8.usereffect_listen_dom_event";
import UseEffectTimerFunction from "./9.usereffect_timer_function";
import UseEffectPreviewAvatar from "./10.useeffect_preview_avatar";
import UseEffectChatApp from "./11.useeffect_fake_chat_socket";
import UseLayoutEffect from "./12.use_layout_effect_in_use";
import UseRef from "./13.useref";
import MemoHigherComponent from "./14.memo";
import UseCallback from "./15.use_callback";
import UseMemoCompo from "./16.useMemo";
import UseReducer from "./17.use_reducer";
import UseReducerExercise from "./18.use_reducer_exercise";

function Hooks() {
  return (
    <div>
      {/* <UserState /> */}
      {/* <UserState2WayBinding /> */}
      {/* <UserStateList /> */}
      <UseStateExercise />
      {/* <UseEffectUpdateDom /> */}
      {/* <UseEffectCallAPI /> */}
      {/* <UseEffectDepenency /> */}
      {/* <UseEffectListenDomEvent /> */}
      {/* <UseEffectTimerFunction /> */}
      {/* <UseEffectPreviewAvatar /> */}
      {/* <UseEffectChatApp /> */}
      {/* <UseLayoutEffect /> */}
      {/* <UseRef /> */}
      {/* <MemoHigherComponent/> */}
      {/* <UseCallback /> */}
      {/* <UseMemoCompo /> */}
      {/* <UseReducer /> */}
      {/* <UseReducerExercise /> */}
    </div>
  );
}

export default Hooks;
