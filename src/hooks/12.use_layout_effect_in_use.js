import React, { useState, useEffect, useLayoutEffect } from "react";

function UseLayoutEffect() {
  const [count, setCount] = useState(0);
  const [countULE, setCountULE] = useState(0);
  // useEffect: load UI before running login
  useEffect(() => {
    if (count > 3) {
      setCount((prev) => (prev = 0));
    }
  }, [count]);

  // userLayoutEffect: load UI after running login
  useLayoutEffect(() => {
    if (countULE > 3) {
      setCountULE((prev) => (prev = 0));
    }
  }, [countULE]);

  const handleCount = () => {
    setCount(count + 1);
    setCountULE(countULE + 1);
  };

  return (
    <div>
      <h1>UseEffect:: {count}</h1>
      <h1>UseLayoutEffect:: {countULE}</h1>
      <button onClick={handleCount}>Counting</button>
    </div>
  );
}

export default UseLayoutEffect;
