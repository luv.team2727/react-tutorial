import React, { useState, useEffect, useLayoutEffect, useRef } from "react";

function UseRef() {
  const [count, setCount] = useState(0);
  // set value
  const timeId = useRef();

  const handleStart = () => {
    clearInterval(timeId.current);
    timeId.current = setInterval(() => {
      setCount((prevCount) => prevCount + 1);
    }, 1000);

    console.log("handle start");
  };

  const handleStop = () => {
    clearInterval(timeId.current);

    console.log("handle stop");
  };

  return (
    <div>
      <h1>{count}</h1>
      <button onClick={handleStart}>Start</button>
      <button onClick={handleStop}>Stop</button>
    </div>
  );
}

export default UseRef;
