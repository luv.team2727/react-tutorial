import React, { useState, useEffect } from "react";

const tabs = ["posts", "comments", "albums"];

function UseEffectDepenency() {
  // useEffect(callback, [deps])
  // usecases
  // 1. useEffect(callback)
  // - Call callback after component re-render
  // - Call callback after component add element into DOM
  // 2. useEffect(callback, [])
  // - Call callback 1 time after component mounted
  // 3. useEffect(callback, [deps])
  // - Callback will called after dependency is changed
  // ===============================
  // 1. Callback always is called when component mounted

  const [title, setTitle] = useState("");
  const [posts, setPosts] = useState([]);
  const [type, setType] = useState("posts");
  console.log("re-render dom");
  // Call API
  useEffect(() => {
    console.log(type);
    fetch(`https://jsonplaceholder.typicode.com/${type}`)
      .then((res) => res.json())
      .then((posts) => {
        setPosts(posts);
      });
  }, [type]);

  return (
    <div>
      {tabs.map((tab) => (
        <button
          key={tab}
          onClick={() => setType(tab)}
          style={
            type === tab
              ? {
                  color: "#fff",
                  backgroundColor: "#333",
                }
              : {}
          }
        >
          {tab}
        </button>
      ))}

      <input value={title} onChange={(e) => setTitle(e.target.value)} />
      <ul>
        {posts.map((post) => {
          return <li key={post.id}> {post.title ?? post.name}</li>;
        })}
      </ul>
    </div>
  );
}

export default UseEffectDepenency;
