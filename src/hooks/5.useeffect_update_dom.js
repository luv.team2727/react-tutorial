import React, { useState, useEffect } from "react";

function UseEffectUpdateDom() {
  // useEffect(callback, [deps])
  // usecases
  // 1. useEffect(callback)
  // - Call callback after component re-render
  // - Call callback after component add element into DOM
  // 2. useEffect(callback, [])
  // 3. useEffect(callback, [deps])
  // ===============================
  // 1. Callback always is called when component mounted
  // 2.

  // Update DOM
  const [count, setCount] = useState(0);
  const [title, setTitle] = useState("");
  // Similar to componentDidMount and componentDidUpdate:
  useEffect(() => {
    // Update the document title using the browser API
    document.title = `You clicked ${count} times`;
  });
  useEffect(() => {
    console.log("Re-render", title);
    //     document.title = title;
  });

  //
  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>Click me</button>

      <p>Set title</p>
      <input value={title} onChange={(e) => setTitle(e.target.value)} />
      {console.log("Render")}
    </div>
  );
}

export default UseEffectUpdateDom;
