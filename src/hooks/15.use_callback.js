import React, { useState, memo, useRef, useCallback } from "react";
import UseCallbackChild from "./15.use_callback_child";

function UseCallback() {
  const [count, setCount] = useState(0);
  // useRef
  const timeId = useRef();
  // useCallback
  const handleStart = useCallback(() => {
    handleStop();
    timeId.current = setInterval(() => {
      setCount((prevCount) => prevCount + 1);
    }, 1000);
    console.log("handle start");
  }, []);
  // useCallback
  const handleStop = useCallback(() => {
    clearInterval(timeId.current);
    console.log("handle stop");
  }, []);

  return (
    <div>
      <h1>{count}</h1>
      <UseCallbackChild onStart={handleStart} onStop={handleStop} />
    </div>
  );
}

export default UseCallback;
