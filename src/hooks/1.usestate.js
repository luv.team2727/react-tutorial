import React, { useState } from "react";

const orders = [100, 200, 300];

function UserState() {
  // usestate + callback inside usestate
  var [count, setCount] = useState(() => {
    const total = orders.reduce((total, cur) => total + cur);
    return total;
  });
  // callback
  const handleIncrease = () => {
    setCount((prevState) => prevState + 1);
    setCount((prevState) => prevState + 1);
    setCount((prevState) => prevState + 1);
  };

  // usestate
  var [info, setInfo] = useState({
    name: "Ngo Tuan A",
    age: 18,
    adress: "Ha Noi",
  });

  const handleUpdate = () => {
    setInfo((prevState) => {
      return {
        ...prevState,
        bio: "Boy",
      };
    });
  };

  return (
    <div className="App" style={{ padding: 32 }}>
      <h1>{count}</h1>
      <button onClick={handleIncrease}>Increase</button>

      <h1>{JSON.stringify(info)}</h1>
      <button onClick={handleUpdate}>Update</button>
    </div>
  );
}

export default UserState;
