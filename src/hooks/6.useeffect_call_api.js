import React, { useState, useEffect } from "react";

function UseEffectCallAPI() {
  // useEffect(callback, [deps])
  // usecases
  // 1. useEffect(callback)
  // - Call callback after component re-render
  // - Call callback after component add element into DOM
  // 2. useEffect(callback, [])
  // - Call callback 1 time after component mounted
  // 3. useEffect(callback, [deps])
  // ===============================
  // 1. Callback always is called when component mounted

  const [title, setTitle] = useState("");
  const [posts, setPosts] = useState([]);
  // Call API
  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((res) => res.json())
      .then((posts) => {
        setPosts(posts);
      });
  }, []);

  //
  return (
    <div>
      <input value={title} onChange={(e) => setTitle(e.target.value)} />
      <ul>
        {posts.map((post) => {
          return <li key={post.id}> {post.title}</li>;
        })}
      </ul>
    </div>
  );
}

export default UseEffectCallAPI;
