import React, { useState } from "react";

function UserStateList() {
  const courses = [
    {
      id: 1,
      name: "html, css",
    },
    {
      id: 2,
      name: "JavaScript",
    },
    {
      id: 3,
      name: "ReactJs",
    },
  ];

  const [checked, setChecked] = useState();
  const handleSubmit = () => {
    console.log(checked);
  };

  const [checkbox, setCheckbox] = useState([]);
  console.log(checkbox);
  const handleCheckbox = (id) => {
    setCheckbox((prev) => {
      const isCheckbox = prev.includes(id);
      if (isCheckbox) {
        return prev.filter((item) => item !== id);
      } else {
        return [...prev, id];
      }
    });
  };

  return (
    <div className="App" style={{ padding: 32 }}>
      {courses.map((course) => (
        <div key={course.id}>
          <input
            type="radio"
            checked={checked === course.id}
            onChange={() => setChecked(course.id)}
          />
          {course.name}
        </div>
      ))}

      {courses.map((course) => (
        <div key={course.id}>
          <input
            type="checkbox"
            checked={checkbox.includes(course.id)}
            onChange={() => handleCheckbox(course.id)}
          />
          {course.name}
        </div>
      ))}

      <button onClick={handleSubmit}>Register</button>
    </div>
  );
}

export default UserStateList;
