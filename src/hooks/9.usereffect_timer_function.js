import React, { useState, useEffect } from "react";

function UseEffectTimerFunction() {
  // useEffect(callback, [deps])
  // usecases
  // 1. useEffect(callback)
  // - Call callback after component re-render
  // - Call callback after component add element into DOM
  // 2. useEffect(callback, [])
  // 3. useEffect(callback, [deps])
  // ===============================
  // 1. Callback always is called when component mounted
  // 2. Cleanup Funtion is called before unmounted

  const [countdown, setCountdown] = useState(180);

  useEffect(() => {
    const timerId = setInterval(() => {
      setCountdown((prevState) => prevState - 1);
      console.log(`countdown::${countdown}`);
    }, 1000);

    // cleanup function
    return () => clearInterval(timerId);
  }, []);

  //
  return (
    <div>
      <h1>{countdown}</h1>
    </div>
  );
}

export default UseEffectTimerFunction;
