import React, { useState, memo, useRef } from "react";

function MemoChild() {
  console.log("re-render in MemoChild component ");

  return <div></div>;
}

export default memo(MemoChild);
