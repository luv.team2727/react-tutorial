import React, { useState, useEffect } from "react";

const lessons = [
  {
    id: 1,
    name: "What is ReactJs?",
  },
  {
    id: 2,
    name: "What is SPA/MPA",
  },
  {
    id: 3,
    name: "Arrow function",
  },
];

function UseEffectChatApp() {
  // useEffect(callback, [deps])
  // usecases
  // 1. useEffect(callback)
  // - Call callback after component re-render
  // - Call callback after component add element into DOM
  // 2. useEffect(callback, [])
  // 3. useEffect(callback, [deps])
  // ===============================
  // 1. Callback always is called when component mounted
  // 2. Cleanup Funtion is called before unmounted
  // 3. Cleanup Function is called before callback

  const [lessionId, setLessionId] = useState(lessons[0].id);

  useEffect(() => {
    const handleComment = ({ detail }) => {
      console.log(detail);
    };

    window.addEventListener(`lesson-${lessionId}`, handleComment);

    return () => {
      window.removeEventListener(`lesson-${lessionId}`, handleComment);
    };
  }, [lessionId]);

  return (
    <div>
      <ul>
        {lessons.map((lesson) => {
          return (
            <li
              key={lesson.id}
              style={{ color: lessionId === lesson.id ? "red" : "#333" }}
              onClick={() => setLessionId(lesson.id)}
            >
              {lesson.name}
            </li>
          );
        })}
      </ul>
    </div>
  );
}

export default UseEffectChatApp;
