import React, { useState, memo, useRef } from "react";
import MemoChild from "./14.memo_child";

function MemoHigherComponent() {
  const [count, setCount] = useState(0);
  // set value
  const timeId = useRef();
  const handleStart = () => {
    timeId.current = setInterval(() => {
      setCount((prevCount) => prevCount + 1);
    }, 1000);
    console.log("handle start");
  };

  const handleStop = () => {
    clearInterval(timeId.current);
    console.log("handle stop");
  };

  return (
    <div>
      <h1>MemoChild is not called in console log</h1>
      <MemoChild />
      <h1>{count}</h1>
      <button onClick={handleStart}>Start</button>
      <button onClick={handleStop}>Stop</button>
    </div>
  );
}

export default MemoHigherComponent;
