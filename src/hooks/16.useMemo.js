import React, { useState, memo, useMemo } from "react";

function UseMemoCompo() {
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [products, setProducts] = useState([]);

  const handleSubmit = () => {
    setProducts([...products, { name, price: +price }]);
  };

  // not useMemo
  const total = products.reduce((result, product) => {
    console.log("Counting again without UseMemo");
    return result + product.price;
  }, 0);

  // useMemo
  const total2 = useMemo(() => {
    const result = products.reduce((result, product) => {
      console.log("Counting again with UseMemo");
      return result + product.price;
    }, 0);

    setName("");
    setPrice("");

    return result;
  }, [products]);

  return (
    <div style={{ padding: "10px 32px" }}>
      <input
        value={name}
        placeholder="Enter name..."
        onChange={(e) => setName(e.target.value)}
      ></input>
      <br />
      <input
        value={price}
        placeholder="Enter price..."
        onChange={(e) => setPrice(e.target.value)}
      ></input>
      <br />
      <button onClick={handleSubmit}>Click</button>
      <br />
      Total: {total2}
      <ul>
        {products.map((product, index) => {
          return (
            <li key={index}>
              {product.name} - {product.price}
            </li>
          );
        })}
      </ul>
    </div>
  );
}

export default UseMemoCompo;
