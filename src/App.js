import logo from "./logo.svg";
import "./App.css";
import React, { useState } from "react";
import Hooks from "./hooks";

function App() {
  const [show, setShow] = useState(false);

  return (
    <div className="App">
      <button onClick={() => setShow(!show)}>Show</button>
      {show && <Hooks />}
    </div>
  );
}

export default App;
